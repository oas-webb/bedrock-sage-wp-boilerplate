<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <title><?php wp_title();?></title>
    <?php if (WP_ENV == 'production') { ?>
        <!-- Google Tag Manager -->
        <!-- End Google Tag Manager -->
    <?php } ?>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<script>
$(document).ready(function() {
    preloaderFadeOutTime = 350;
    function hidePreloader() {
        var preloader = $('.spinner-wrapper');
        preloader.fadeOut(preloaderFadeOutTime);
    }
    setTimeout(function() {
        hidePreloader();
    }, 650);
});
</script>

<!--CSS Spinner-->
<div class="spinner-wrapper">
    <!-- Place page transition animation here -->
</div>

<?php if (WP_ENV == 'production') { ?>
    <!-- Google Tag Manager (noscript) -->
    <!-- End Google Tag Manager (noscript) -->
<?php } ?>

<header>
    <h3>Header</h3>
</header>