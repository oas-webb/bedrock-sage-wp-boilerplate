<?php

// Add styles and scripts
function oas_scripts() {

  // CSS
  wp_enqueue_style ( 'oas_styles', get_template_directory_uri() . '/watch/scripts.css' );

  // JS
  wp_enqueue_script ( 'oas_scripts_jquery', get_template_directory_uri() . '/watch/jquery.js' );
  wp_enqueue_script ( 'oas_scripts', get_template_directory_uri() . '/watch/scripts.js' );

  wp_localize_script('oas_scripts', 'wpAjax', array('ajaxUrl' => admin_url('admin-ajax.php')));

}
add_action('wp_enqueue_scripts', 'oas_scripts');

// Add options ACF
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

// Add menus
function oas_menu() {
  register_nav_menu('oas_menu',__( 'OAS menu' ));
}
add_action( 'init', 'oas_menu' );

// Register ACF blocks
if (function_exists('acf_register_block_type')) {
  add_action('acf/init', 'register_acf_block_types');
}
function register_acf_block_types() {
  // acf_register_block_type (
  //   array(
  //     'name' => 'frontpage_hero',
  //     'title' => __('Frontpage Hero'),
  //     'description' => __('Frontpage Hero section'),
  //     'render_template' => 'template_parts/blocks/frontpage-hero.php',
  //     'keywords' => array('jumbotron', 'hero')
  //   )
  // );
  // acf_register_block_type (
  //   array(
  //     'name' => 'page_hero',
  //     'title' => __('Page Hero'),
  //     'description' => __('Page Hero section'),
  //     'render_template' => 'template_parts/blocks/page-hero.php',
  //     'keywords' => array('jumbotron', 'hero', 'page')
  //   )
  // );
  // acf_register_block_type (
  //   array(
  //     'name' => 'call_to_action',
  //     'title' => __('Call to action'),
  //     'description' => __('CTA block, bild och text'),
  //     'render_template' => 'template_parts/blocks/call-to-action.php',
  //     'keywords' => array('text', 'media', 'cta')
  //   )
  // );
  // acf_register_block_type (
  //   array(
  //     'name' => 'news_row',
  //     'title' => __('News row'),
  //     'description' => __('3 Nyheter i rad. Mer eller färre. Du bestämmer.'),
  //     'render_template' => 'template_parts/blocks/news-row.php',
  //     'keywords' => array('cards', 'news')
  //   )
  // );
  // acf_register_block_type (
  //   array(
  //     'name' => 'offices_page',
  //     'title' => __('Offices page'),
  //     'description' => __('Lägg in kontor.'),
  //     'render_template' => 'template_parts/blocks/offices-page.php',
  //     'keywords' => array('kontor', 'office')
  //   )
  // );
  // acf_register_block_type (
  //   array(
  //     'name' => 'employees',
  //     'title' => __('Employees'),
  //     'description' => __('Lägg in anställda.'),
  //     'render_template' => 'template_parts/blocks/employees.php',
  //     'keywords' => array('employees', 'office')
  //   )
  // );
}

// Register post thumb
add_theme_support( 'post-thumbnails' );

// Register custom post type
// function news_post_type() {
//     register_post_type('news',
//         array(
//             'labels'      => array(
//                 'name'          => __( 'Nyheter' ),
//                 'singular_name' => __( 'nyhet' ),
//             ),
//             'public'      => true,
//             'has_archive' => true,
//             'rewrite'     => array( 'slug' => 'nyheter' ),
//             'supports' => array(
//               'title',
//               'editor',
//               'thumbnail',
//               'title',
//               'editor',
//               'author',
//               'excerpt',
//               'trackbacks',
//               'custom-fields',
//               'comments',
//               'revisions',
//               'page-attributes',
//               'post-formats',
//             ),
//             'show_in_rest' => true,
//         )
//     );
// }
// add_action('init', 'news_post_type');

// Register taxanomy
// function themes_taxonomy() {
//     register_taxonomy(
//         'categories',
//         'news',
//         array(
//             'hierarchical' => true,
//             'label' => 'Kategorier',
//             'query_var' => true,
//             'rewrite' => array(
//                 'slug' => 'kategorier',
//                 'with_front' => false
//             )
//         )
//     );
// }
// add_action( 'init', 'themes_taxonomy');

// Remove default post
// add_action( 'admin_menu', 'remove_default_post_type' );

// function remove_default_post_type() {
//     remove_menu_page( 'edit.php' );
// }

// Ajax filter query
add_action( 'wp_ajax_nopriv_filter', 'filter_ajax' );
add_action( 'wp_ajax_filter', 'filter_ajax' );

function filter_ajax() {

  $category = $_POST['category'];

  $args = array(
    'post_type' => 'post',
    'posts_per_page' => -1
  );

  if(isset($category)) {
    $args['category__in'] = array($category);
  }

  $query = new WP_Query($args);

  if($query->have_posts()) :
    while($query->have_posts()) : $query->the_post();
      $title_trimmed = wp_trim_words( get_the_title(), 6, '...' );
      ?>
        <div class="news-item">
          <a href="<?php the_permalink(); ?>">
            <div class="image-wrapper">
              <?php the_post_thumbnail(); ?>
            </div>
            <h4><?php echo esc_html( $title_trimmed ); ?></h4>
            <div class="button-wrapper with_arrow">
              <div class="btn secondary">
                <span>Read more</span>
              </div>
            </div>
          </a>
        </div>
      <?php
    endwhile;
  endif;

  wp_reset_postdata();

  die();

}